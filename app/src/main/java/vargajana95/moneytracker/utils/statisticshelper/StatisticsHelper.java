package vargajana95.moneytracker.utils.statisticshelper;

import java.util.ArrayList;

import vargajana95.moneytracker.model.Item;

/**
 * Created by Varga János on 2017. 10. 29..
 */

public class StatisticsHelper {

    public static int calculateSum(ArrayList<Item> transactions) {
        int sum = 0;

        for(Item transaction : transactions) {
            if (transaction.isIncome())
                sum += transaction.getValue();
            else
                sum -= transaction.getValue();
        }
        return sum;
    }
}
