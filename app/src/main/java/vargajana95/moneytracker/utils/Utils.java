package vargajana95.moneytracker.utils;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class Utils {

    public static boolean areDatesEqual(Calendar date1, Calendar date2) {
        return (date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR) &&
                date1.get(Calendar.MONTH) == date2.get(Calendar.MONTH) &&
                date1.get(Calendar.DAY_OF_MONTH) == date2.get(Calendar.DAY_OF_MONTH));
    }

    public static String getDateFrom(Calendar date) {
        return String.format(Locale.getDefault(), "%s, %s %02d.",
                date.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()),
                date.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()),
                date.get(Calendar.DAY_OF_MONTH));
    }

}
