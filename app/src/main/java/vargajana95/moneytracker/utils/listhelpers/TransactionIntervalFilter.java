package vargajana95.moneytracker.utils.listhelpers;

import java.util.Calendar;

import vargajana95.moneytracker.model.Item;

/**
 * Created by Varga János on 2017. 10. 29..
 */

public class TransactionIntervalFilter implements Filter<Item> {
    private Calendar startDate;
    private Calendar endDate;

    public TransactionIntervalFilter(Calendar startDate, Calendar endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public boolean needToAdd(Item o) {
        return (o.getDate().before(endDate) && o.getDate().after(startDate));
    }
}

