package vargajana95.moneytracker.utils.listhelpers;

import java.util.ArrayList;

import vargajana95.moneytracker.model.Category;

/**
 * Created by Varga János on 2017. 10. 28..
 */


public class ListFilterHelper {
    public static <T> ArrayList<T> filter(ArrayList<T> list, Filter filter) {
        ArrayList<T> ret = new ArrayList<>();
        for(T o : list) {
            if (filter.needToAdd(o)) {
                ret.add(o);
            }
        }
        return ret;
    }


    public static ArrayList<String> getCategoryNames(ArrayList<Category> categories) {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories)
            categoryNames.add(category.getName());


        return categoryNames;
    }
}
