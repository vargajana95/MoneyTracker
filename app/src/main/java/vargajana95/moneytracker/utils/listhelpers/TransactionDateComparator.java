package vargajana95.moneytracker.utils.listhelpers;

import java.util.Comparator;

import vargajana95.moneytracker.model.Item;

/**
 * Created by Varga János on 2017. 11. 21..
 */

public class TransactionDateComparator implements Comparator<Item> {

    @Override
    public int compare(Item i1, Item i2) {
        return i1.getDate().compareTo(i2.getDate());
    }
}
