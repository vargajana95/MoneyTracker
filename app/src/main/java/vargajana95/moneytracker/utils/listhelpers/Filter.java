package vargajana95.moneytracker.utils.listhelpers;

/**
 * Created by Varga János on 2017. 10. 28..
 */

public interface Filter<T> {
    boolean needToAdd(T o);
}
