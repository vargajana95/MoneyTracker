package vargajana95.moneytracker.utils.listhelpers;

import vargajana95.moneytracker.model.Category;

/**
 * Created by Varga János on 2017. 10. 28..
 */

public class CategoryIsIncomeFilter implements Filter<Category> {
    private boolean isIncome;

    public CategoryIsIncomeFilter(boolean isIncome) {
        this.isIncome = isIncome;
    }

    @Override
    public boolean needToAdd(Category o) {
        //TODO lehetne szebben
        if (isIncome)
            return o.isIncome();
        else
            return !o.isIncome();
    }
}
