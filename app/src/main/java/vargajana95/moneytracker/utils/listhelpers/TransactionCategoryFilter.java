package vargajana95.moneytracker.utils.listhelpers;

import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.Item;

/**
 * Created by Varga János on 2017. 11. 13..
 */

public class TransactionCategoryFilter implements Filter<Item> {
    private Category category;

    public TransactionCategoryFilter(Category category) {
        this.category = category;
    }

    @Override
    public boolean needToAdd(Item o) {
        return category == null || o.getCategory().equals(category);
    }
}