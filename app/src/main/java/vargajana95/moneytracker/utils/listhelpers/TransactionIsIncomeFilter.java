package vargajana95.moneytracker.utils.listhelpers;

import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.Item;

/**
 * Created by Varga János on 2017. 10. 29..
 */

public class TransactionIsIncomeFilter implements Filter<Item> {
    private boolean isIncome;

    public TransactionIsIncomeFilter(boolean isIncome) {
        this.isIncome = isIncome;
    }

    @Override
    public boolean needToAdd(Item o) {
        //TODO lehetne szebben
        if (isIncome)
            return o.isIncome();
        else
            return !o.isIncome();
    }
}
