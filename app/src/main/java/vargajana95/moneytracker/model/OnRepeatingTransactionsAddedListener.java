package vargajana95.moneytracker.model;

import java.util.ArrayList;

/**
 * Created by Varga János on 2017. 10. 26..
 */

public interface OnRepeatingTransactionsAddedListener {
    void onRepeatingTransactionsAdded(ArrayList<RepeatingTransactionToAdd> items);
}
