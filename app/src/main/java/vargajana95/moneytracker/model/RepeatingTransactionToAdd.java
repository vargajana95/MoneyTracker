package vargajana95.moneytracker.model;

/**
 * Created by Varga János on 2017. 10. 28..
 */

public class RepeatingTransactionToAdd {
    private boolean checked;
    private Item item;

    public RepeatingTransactionToAdd(Item item) {
        this.item = item;
        checked = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public Item getItem() {
        return item;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
