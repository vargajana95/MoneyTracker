package vargajana95.moneytracker.model;

/**
 * Created by Varga János on 2017. 10. 23..
 */

public interface Observer {
    void onItemAdded(Item item);
    void onItemDeleted(Item item);
    void onItemEdited(Item newItem, int position);
}
