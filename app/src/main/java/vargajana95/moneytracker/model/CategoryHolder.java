package vargajana95.moneytracker.model;

import java.util.ArrayList;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class CategoryHolder {
    //TODO: talan mashol kellene lennie
    private ArrayList<Category> categories = new ArrayList<>();
    private static CategoryHolder instance;

    private CategoryHolder() {
        categories.addAll(Category.listAll(Category.class));
    }

    public static CategoryHolder getInstance() {
        if (instance == null) {
            instance = new CategoryHolder();
        }
        return instance;
    }

    public void addCategory(Category cat) {
        categories.add(cat);
        cat.save();
    }

    public ArrayList<String> getCategories() {
        ArrayList<String> categoryNames = new ArrayList<>();

        for (Category category : categories)
            categoryNames.add(category.getName());

        return categoryNames;
    }

    public ArrayList<Category> getCategoryList() {
        return (ArrayList<Category>) categories.clone();
    }

    public Category get(int i) {
        return categories.get(i);
    }

    public Category get(String s) {
        for (Category category : categories) {
            if (category.getName().equals(s))
                return category;
        }
        return null;
    }

    public void removeCategory(Category category) {
        categories.remove(category);
        category.delete();
    }
    public int getCategoryPosition(Category category) {
        return categories.indexOf(category);
    }

}
