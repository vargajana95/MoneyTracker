package vargajana95.moneytracker.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

/**
 * Created by Varga János on 2017. 10. 26..
 */

public class RepeatingTransactionsManager {
    private OnRepeatingTransactionsAddedListener listener;

    public RepeatingTransactionsManager(OnRepeatingTransactionsAddedListener listener) {
        this.listener = listener;
    }

    public void check() {
        ArrayList<RepeatingTransaction> transactions = ItemHolder.getInstance().getRepeatingTransactions();
        ArrayList<RepeatingTransactionToAdd> items = new ArrayList<>();
        for (RepeatingTransaction  transaction : transactions) {
            while (transaction.getNextAdded().before(Calendar.getInstance())) {
                Calendar newNextAdded = transaction.getNextAdded();
                Item item = (Item) transaction.getItem().clone();
                item.setDate(newNextAdded.get(Calendar.YEAR), newNextAdded.get(Calendar.MONTH), newNextAdded.get(Calendar.DAY_OF_MONTH));
                //noinspection WrongConstant
                newNextAdded.add(transaction.getRepeatType(), 1);
                transaction.setNextAdded(newNextAdded);
                items.add(new RepeatingTransactionToAdd(item));
                //ItemHolder.getInstance().addItem(item);
                if (transaction.isRepeatTimesEnabled()) {
                    transaction.decreaseRepeateNum();
                    transaction.save();

                    if (transaction.getRepeatNum() == 0) {
                        ItemHolder.getInstance().removeRepeatingTransaction(transaction);
                        break;
                    }
                } else
                    transaction.save();
            }

        }

        if (!items.isEmpty()) {
            listener.onRepeatingTransactionsAdded(items);
        }
    }


}
