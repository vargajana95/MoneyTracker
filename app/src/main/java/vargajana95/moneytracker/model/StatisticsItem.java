package vargajana95.moneytracker.model;

/**
 * Created by Varga János on 2017. 10. 23..
 */

public class StatisticsItem {
    public Category category;
    public int sum;
    public double percent;

    public StatisticsItem(Category category) {
        this.category = category;
    }
}
