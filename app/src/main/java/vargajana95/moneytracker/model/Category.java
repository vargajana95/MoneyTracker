package vargajana95.moneytracker.model;


import com.orm.SugarRecord;


/**
 * Created by Varga János on 2017. 10. 21..
 */

public class Category extends SugarRecord implements Cloneable{
    private String name;
    private boolean isIncome;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public Category(String name, boolean isIncome) {
        this.name = name;
        this.isIncome = isIncome;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isIncome() {
        return isIncome;
    }

    public void setIncome(boolean income) {
        isIncome = income;
    }

}
