package vargajana95.moneytracker.model;

import com.orm.SugarRecord;

import java.util.Calendar;
import java.util.Objects;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class Item extends SugarRecord implements Cloneable {
    private String name;
    private int value;
    private String comment;
    private Category category;
    private boolean income;
    private Calendar date;


    public Item() {
        this.date = Calendar.getInstance();
    }

    public Item(String name, int value, String comment, Category category) {
        this.name = name;
        this.value = value;
        this.comment = comment;
        this.category = category;
        this.date = Calendar.getInstance();
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        category.save();
        this.category = category;
    }

    public boolean isIncome() {
        return income;
    }

    public void setIncome(boolean income) {
        this.income = income;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(int year, int month, int day) {
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);
    }
    @Override
    public Object clone() {
        Item item = new Item();
        item.name = name;
        item.value = value;
        item.comment = comment;
        item.category = category;
        item.income = income;
        item.date = (Calendar) date.clone();
        return item;
    }
}
