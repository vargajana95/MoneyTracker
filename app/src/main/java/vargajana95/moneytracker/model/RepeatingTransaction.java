package vargajana95.moneytracker.model;

import com.orm.SugarApp;
import com.orm.SugarRecord;

import java.util.Calendar;

/**
 * Created by Varga János on 2017. 10. 26..
 */

public class RepeatingTransaction extends SugarRecord {
    private Calendar nextAdded;
    private int repeatType;

    private String name;
    private int value;
    private String comment;
    private Category category;
    private boolean income;
    private int repeatNum;
    private boolean isRepeatTimesEnabled;

    //SugarORM
    public RepeatingTransaction() {
    }

    public RepeatingTransaction(Item item) {
        this.name = item.getName();
        this.comment = item.getComment();
        this.category = item.getCategory();
        this.income = item.isIncome();
        this.value = item.getValue();
    }

    public RepeatingTransaction(Item item, Calendar nextAdded, int repeatType) {
        this.name = item.getName();
        this.comment = item.getComment();
        this.category = item.getCategory();
        this.income = item.isIncome();
        this.value = item.getValue();

        this.nextAdded = nextAdded;
        this.repeatType = repeatType;
    }

    public Item getItem() {
        Item item = new Item(name, value, comment, category);
        item.setIncome(income);
        return item;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setIncome(boolean income) {
        this.income = income;
    }

    public boolean isRepeatTimesEnabled() {
        return isRepeatTimesEnabled;
    }

    public void setRepeatTimesEnabled(boolean repeatTimesEnabled) {
        isRepeatTimesEnabled = repeatTimesEnabled;
    }

    public void setRepeatNum(int repeatNum) {
        this.repeatNum = repeatNum;
    }

    public int getRepeatNum() {
        return repeatNum;
    }

    public void decreaseRepeateNum() {
        repeatNum--;
    }

    public Calendar getNextAdded() {
        return (Calendar) nextAdded.clone();
    }

    public int getRepeatType() {
        return repeatType;
    }

    public void setNextAdded(Calendar nextAdded) {
        this.nextAdded = nextAdded;
    }

    public void setRepeatType(int repeatType) {
        this.repeatType = repeatType;
    }
    public String getRepeatTypeString() {
        if (repeatType == Calendar.YEAR)
            return "Évente";
        else if (repeatType == Calendar.MONTH)
            return "Havonta";
        else if (repeatType == Calendar.WEEK_OF_YEAR)
            return "Hetente";

        return "";
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getComment() {
        return comment;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isIncome() {
        return income;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
