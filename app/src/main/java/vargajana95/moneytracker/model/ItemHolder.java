package vargajana95.moneytracker.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

import vargajana95.moneytracker.utils.Utils;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class ItemHolder {
    private static ItemHolder instance = null;
    private ArrayList<Item> items;
    private ArrayList<Observer> observers;
    private ArrayList<RepeatingTransaction> repeatingTransactions;

    protected ItemHolder() {
        items = new ArrayList<>();
        items.addAll(Item.listAll(Item.class));

        for (Item item : items) {
            for (Category category : CategoryHolder.getInstance().getCategoryList()) {
                if (category.getId().equals(item.getCategory().getId())) {
                    item.setCategory(category);
                    break;
                }
            }
        }
        observers = new ArrayList<>();
        repeatingTransactions = new ArrayList<>();

        repeatingTransactions.addAll(RepeatingTransaction.listAll(RepeatingTransaction.class));

        for (RepeatingTransaction repeatingTransaction : repeatingTransactions) {
            for (Category category : CategoryHolder.getInstance().getCategoryList()) {
                if (category.getId().equals(repeatingTransaction.getCategory().getId())) {
                    repeatingTransaction.setCategory(category);
                    break;
                }
            }
        }

        /*Item item;


        item = new Item("Repeating1", 20000, "Rep", CategoryHolder.get(2));
        Calendar c = Calendar.getInstance();
        c.set(2017, 9, 28);
        addRepeatingTransaction(new RepeatingTransaction(item, c, Calendar.WEEK_OF_YEAR));*/
    }

    public static ItemHolder getInstance() {
        if (instance == null) {
            instance = new ItemHolder();
        }

        return instance;
    }

    public void addItem(Item item) {
        items.add((Item) item.clone());
        item.save();

        for (Observer observer : observers) {
            observer.onItemAdded((Item) item.clone());
        }
    }

    public void removeTransaction(Item transaction) {
        items.remove(transaction);
        transaction.delete();
        for (Observer observer : observers)
            observer.onItemDeleted((Item) transaction.clone());
    }

    public Item getItem(int i) {
        return items.get(i);
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public ArrayList<Item> getItemsByDate(Calendar date) {
        ArrayList<Item> dateItems = new ArrayList<>();
        for (Item item : items) {
            if (Utils.areDatesEqual(item.getDate(), date)) {
                dateItems.add(item);
            }
        }
        return dateItems;
    }
    public ArrayList<Item> getItemsByType(int field, Calendar date) {
        return getItemsByType(field, date, null);
    }

    //Ha date == null akkor nem néz dátumot, ha category == null akkor nem néz kategóriát
    public ArrayList<Item> getItemsByType(int field, Calendar date, Category category) {
        ArrayList<Item> filteredItems = new ArrayList<>();
        for (Item item : items) {
            if (date == null || item.getDate().get(Calendar.YEAR) == date.get(Calendar.YEAR) &&
                    item.getDate().get(field) == date.get(field)) {
                    if (category == null || item.getCategory().equals(category))
                        filteredItems.add(item);

            }
        }
        return filteredItems;
    }

    public void addRepeatingTransaction(RepeatingTransaction rTransaction) {
        repeatingTransactions.add(rTransaction);
        rTransaction.save();
    }

    public void removeRepeatingTransaction(RepeatingTransaction rTransaction) {
        repeatingTransactions.remove(rTransaction);
        rTransaction.delete();
    }

    public ArrayList<RepeatingTransaction> getRepeatingTransactions() {
        return (ArrayList<RepeatingTransaction>) repeatingTransactions.clone();
    }

    public ArrayList<Item> listAll() {
        return (ArrayList<Item>) items.clone();
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public int getItemIndex(Item transaction) {
        return items.indexOf(transaction);
    }

    public RepeatingTransaction getRepeatingTransaction(int index) {
        return repeatingTransactions.get(index);
    }

    public int getRepeatingTransactionIndex(RepeatingTransaction transaction) {
        return repeatingTransactions.indexOf(transaction);
    }

    public void editItem(Item newTransaction, int position) {
        if (position >= items.size())
            return;

        items.get(position).delete();
        Item transaction = (Item) newTransaction.clone();
        items.set(position, transaction);
        transaction.save();

        for (Observer observer : observers)
            observer.onItemEdited((Item) newTransaction.clone(), position);
    }
}
