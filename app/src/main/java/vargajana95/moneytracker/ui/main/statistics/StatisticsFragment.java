package vargajana95.moneytracker.ui.main.statistics;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.Observer;
import vargajana95.moneytracker.model.StatisticsItem;
import vargajana95.moneytracker.utils.Utils;
import vargajana95.moneytracker.utils.listhelpers.CategoryIsIncomeFilter;
import vargajana95.moneytracker.utils.listhelpers.ListFilterHelper;
import vargajana95.moneytracker.utils.listhelpers.TransactionIsIncomeFilter;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class StatisticsFragment extends Fragment implements Observer{
    private RecyclerView recyclerView;
    private StatisticsAdapter statisticsAdapter;
    private Calendar filterDate;
    private Button btnDate;
    private ImageButton btnPrevDay;
    private ImageButton btnNextDay;

    private int filterDateType;
    private boolean filterAllDates;


    private ItemHolder itemHolder;
    private boolean filterByIncome;

    private TextView tvSum;

    public StatisticsFragment() {
        filterDate = Calendar.getInstance();
        itemHolder = ItemHolder.getInstance();
        filterDateType = Calendar.WEEK_OF_YEAR;

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
       filterCategories();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.findItem(R.id.menu_date).setVisible(true);
        menu.findItem(R.id.menu_category).setVisible(false);
        menu.findItem(R.id.menu_add).setVisible(false);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_statistics, container, false);
        btnPrevDay = view.findViewById(R.id.btnPrevDay);
        btnNextDay = view.findViewById(R.id.btnNextDay);
        btnDate = view.findViewById(R.id.tvDate);
        btnPrevDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDate.add(filterDateType, -1);
                filterCategories();
            }
        });
        btnNextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDate.add(filterDateType, 1);
                filterCategories();
            }
        });
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    filterByIncome = false;
                } else {
                    filterByIncome = true;
                }
                //statisticsAdapter.itemsChanged(calculateStatistics());
                filterCategories();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        tvSum = view.findViewById(R.id.tvSum);


        initRecyclerView(view);

        itemHolder.addObserver(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        itemHolder.removeObserver(this);
    }


    /*private ArrayList<StatisticsItem> calculateStatistics() {
        //Filter by date and income/expense
        ArrayList<Item> transactions = ListFilterHelper.filter(itemHolder.getItemsByType(filterDateType, filterDate),
                new TransactionIsIncomeFilter(filterByIncome));

        return calculateStatistics(transactions);
    }*/

    private ArrayList<StatisticsItem> calculateStatistics() {
        ArrayList<Item> transactions;
        if (!filterAllDates) {
            transactions = ListFilterHelper.filter(itemHolder.getItemsByType(filterDateType, filterDate),
                    new TransactionIsIncomeFilter(filterByIncome));
        } else {
            transactions = ListFilterHelper.filter(ItemHolder.getInstance().getItems(),
                                            new TransactionIsIncomeFilter(filterByIncome));
        }

        ArrayList<StatisticsItem> stat = new ArrayList<>();

        ArrayList<Category> categories = ListFilterHelper.filter(CategoryHolder.getInstance().getCategoryList(),
                            new CategoryIsIncomeFilter(filterByIncome));

        for (Category category : categories) {
            stat.add(new StatisticsItem(category));
        }
        int sum = 0;
        for (Item item : transactions) {
            sum += item.getValue();
            for (StatisticsItem statItem : stat) {
                if (statItem.category.equals(item.getCategory())) {
                    statItem.sum += item.getValue();
                    break;
                }
            }
        }
        tvSum.setText(getString(R.string.currency, sum));

        for (StatisticsItem statItem : stat)
        {
            if (sum != 0)
                statItem.percent = (double)statItem.sum*100 / sum;
        }
        return stat;
    }


    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.StatisticsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        statisticsAdapter = new StatisticsAdapter(getContext());

        recyclerView.setAdapter(statisticsAdapter);
    }

    private void filterCategories() {
        statisticsAdapter.itemsChanged(calculateStatistics());
        if (!filterAllDates) {
            btnNextDay.setEnabled(true);
            btnPrevDay.setEnabled(true);
            if (filterDateType == Calendar.MONTH)
                btnDate.setText(filterDate.getDisplayName(filterDateType, Calendar.LONG, Locale.getDefault()));
            else if (filterDateType == Calendar.WEEK_OF_YEAR)
                btnDate.setText(getString(R.string.week_of_year, filterDate.get(filterDateType)));
            else
                btnDate.setText(String.format(Locale.getDefault(), "%d", filterDate.get(filterDateType)));
        } else {
            btnDate.setText(getString(R.string.list_all));
            btnNextDay.setEnabled(false);
            btnPrevDay.setEnabled(false);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_list_all) {
            filterAllDates = true;
            filterCategories();
            return true;
        }
        else {
            filterAllDates = false;
        }
        if (id == R.id.action_year) {
            filterDateType = Calendar.YEAR;
            filterCategories();
            return true;
        }
        else if (id == R.id.action_month) {
            filterDateType = Calendar.MONTH;
            filterCategories();
            return true;
        }
        else if (id == R.id.action_week) {
            filterDateType = Calendar.WEEK_OF_YEAR;
            filterCategories();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemAdded(Item item) {
        statisticsAdapter.itemsChanged( calculateStatistics() );
    }

    @Override
    public void onItemDeleted(Item item) {
        statisticsAdapter.itemsChanged( calculateStatistics() );

    }

    @Override
    public void onItemEdited(Item newItem, int position) {
        statisticsAdapter.itemsChanged( calculateStatistics() );
    }
}
