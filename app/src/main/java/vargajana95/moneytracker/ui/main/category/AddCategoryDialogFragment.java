package vargajana95.moneytracker.ui.main.category;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;


/**
 * Created by Varga János on 2017. 10. 28..
 */

public class AddCategoryDialogFragment extends AppCompatDialogFragment {
    public static final String TAG = "AddCategoryDialogFragment";

    private RadioButton radioIncome;
    private EditText etName;
    private OnCategoryAddedListener listener;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        if (getActivity() instanceof OnCategoryAddedListener) {
            listener = (OnCategoryAddedListener) getActivity();
        } else {
            throw new RuntimeException("Activity must implement OnCategoryAddedListener interface!");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstance) {
        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.new_category_dialog_title)
                .setView(getContentView())
                .setPositiveButton(R.string.new_category_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onCategoryAdded(getCategory());
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();
    }

    private Category getCategory() {
        Category category = new Category(etName.getText().toString());
        category.setIncome(radioIncome.isChecked());
        return category;
    }
    private View getContentView() {
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_category, null);
        etName = contentView.findViewById(R.id.etName);
        radioIncome = contentView.findViewById(R.id.radioIncome);
        radioIncome.setChecked(true);

        return contentView;
    }

    public interface OnCategoryAddedListener {
        void onCategoryAdded(Category category);
    }
}