package vargajana95.moneytracker.ui.main.summary;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.Observer;
import vargajana95.moneytracker.ui.main.details.DetailsActivity;
import vargajana95.moneytracker.utils.Utils;
import vargajana95.moneytracker.utils.statisticshelper.StatisticsHelper;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class MainFragment extends Fragment implements Observer {
    private RecyclerView recyclerView;
    private ItemAdapter itemAdapter;
    private Calendar filterDate;
    private Button tvDate;

    private ArrayList<Item> items;
    private ItemHolder itemHolder;

    private TextView tvSum;

    //private BalanceManager balanceManager;

    public MainFragment() {
        filterDate = Calendar.getInstance();
        itemHolder = ItemHolder.getInstance();
        items = itemHolder.getItemsByDate(Calendar.getInstance());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getActivity() instanceof WeatherDataHolder) {
            weatherDataHolder = (WeatherDataHolder) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement WeatherDataHolder interface!");
        }*/
        setHasOptionsMenu(true);


        //balanceManager = new BalanceManager();
        //balanceManager.setBalanceListener(this);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.findItem(R.id.menu_category).setVisible(false);
        menu.findItem(R.id.menu_date).setVisible(false);
        menu.findItem(R.id.menu_add).setVisible(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ImageButton btnPrevDay = view.findViewById(R.id.btnPrevDay);
        ImageButton btnNextDay = view.findViewById(R.id.btnNextDay);
        tvDate = view.findViewById(R.id.tvDate);
        btnPrevDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDate.add(Calendar.DAY_OF_MONTH, -1);
                filterDateChanged(filterDate);
                tvDate.setText(Utils.getDateFrom(filterDate));
            }
        });
        btnNextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDate.add(Calendar.DAY_OF_MONTH, 1);
                filterDateChanged(filterDate);
                tvDate.setText(Utils.getDateFrom(filterDate));
            }
        });

        tvDate.setText(Utils.getDateFrom(filterDate));

        tvSum = view.findViewById(R.id.tvSum);


        setSumText(StatisticsHelper.calculateSum(items));
        initRecyclerView(view);

        itemHolder.addObserver(this);

        //balanceManager.setProgressBar(progressBar);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BalanceManager balanceManager = new BalanceManager();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        balanceManager.setListener(new BalanceManager.BalanceListener() {
            @Override
            public void onBalanceIsUsed() {
                View view = getActivity().findViewById(R.id.coordinatorLayout);
                Snackbar snackbar = Snackbar.make(view, R.string.blance_ran_out_text, Snackbar.LENGTH_LONG);

                snackbar.show();
            }

            @Override
            public void onFrameIsUsed() {
                View view = getActivity().findViewById(R.id.coordinatorLayout);
                Snackbar snackbar = Snackbar.make(view, R.string.frame_ran_out_text, Snackbar.LENGTH_LONG);

                snackbar.show();
            }
        });
        ft.add(R.id.balance_manager, balanceManager, BalanceManager.TAG);
        ft.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        filterDateChanged(filterDate);

        //initBalanceManager();

        //balanceManager.check();
    }

    @Override
    public void onPause() {
        super.onPause();
        //balanceManager.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        itemHolder.removeObserver(this);
    }


    private void filterDateChanged(Calendar date) {
        items = itemHolder.getItemsByDate(date);
        itemAdapter.itemsChanged(items);
        setSumText(StatisticsHelper.calculateSum(items));
    }

    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.MainRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        itemAdapter = new ItemAdapter(this.getContext(), new ItemAdapter.OnTransactionSelectedListener() {
            @Override
            public void onTransactionSelected(Item transaction) {
                int index = itemHolder.getItemIndex(transaction);
                Intent showDetailsIntent = new Intent(getActivity(), DetailsActivity.class);
                showDetailsIntent.putExtra(DetailsActivity.EXTRA_TRANSACTION_INDEX, index);
                startActivityForResult(showDetailsIntent, DetailsActivity.DELETE_RESULT);
            }
        });
        itemAdapter.itemsChanged(items);

        recyclerView.setAdapter(itemAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DetailsActivity.DELETE_RESULT) {
            if (resultCode == DetailsActivity.RESULT_OK) {
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.delete_success, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onItemAdded(Item item) {
        if (!Utils.areDatesEqual(item.getDate(), filterDate)) {
            filterDate = (Calendar) item.getDate().clone();
            filterDateChanged(filterDate);
            tvDate.setText(Utils.getDateFrom(filterDate));
        } else {
            itemAdapter.ItemAdded(item);
            items.add(item);
        }

        setSumText(StatisticsHelper.calculateSum(items));
    }

    @Override
    public void onItemDeleted(Item item) {
        if (Utils.areDatesEqual(item.getDate(), filterDate)) {
            filterDateChanged(filterDate);
        }

        setSumText(StatisticsHelper.calculateSum(items));
    }

    @Override
    public void onItemEdited(Item newItem, int position) {
        filterDateChanged(filterDate);


        setSumText(StatisticsHelper.calculateSum(items));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_add)
        {
            AddItemDialogFragment addDialog = new AddItemDialogFragment();
            addDialog.setDate(filterDate);
            addDialog.show(getFragmentManager(), AddItemDialogFragment.TAG);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSumText(int sum) {
        if (sum < 0) {
            sum = -sum;
            tvSum.setTextColor(ContextCompat.getColor(getContext(), R.color.colorRed));
        }
        else {
            tvSum.setTextColor(ContextCompat.getColor(getContext(), R.color.colorGreen));
        }
        tvSum.setText(getString(R.string.currency, sum));

    }

}
