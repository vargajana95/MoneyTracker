package vargajana95.moneytracker.ui.main.details;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.Observer;
import vargajana95.moneytracker.ui.main.MainActivity;
import vargajana95.moneytracker.utils.Utils;

public class DetailsActivity extends AppCompatActivity
        implements EditDialogFragment.OnTransactionEditedListener, Observer{
    public static final String TAG = "DetailsActivity";
    public static final int DELETE_RESULT  = 5000;
    public static final String EXTRA_TRANSACTION_INDEX= "extra.index";

    private Item transaction;
    private TextView tvName;
    private TextView tvCategory;
    private ImageView imIsIncome;
    private TextView tvDate;
    private TextView tvComment;
    private TextView tvValue;
    private int transactionPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        int index = getIntent().getIntExtra(EXTRA_TRANSACTION_INDEX, 0);
        transactionPosition = index;
        transaction = ItemHolder.getInstance().getItem(index);
        setTitle(transaction.getName());

        tvName = (TextView) findViewById(R.id.tvName);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        imIsIncome = (ImageView) findViewById(R.id.imIsIncome);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvComment = (TextView) findViewById(R.id.tvComment);
        tvValue = (TextView) findViewById(R.id.tvValue);


        updateFields();
    }

    @Override
    protected void onResume() {
        super.onResume();

        ItemHolder.getInstance().addObserver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ItemHolder.getInstance().removeObserver(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_delete) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_delete_transaction_title)
                    .setMessage(R.string.dialog_delete_transaction_text)
                    .setPositiveButton(R.string.dialog_delete_transaction_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ItemHolder.getInstance().removeTransaction(transaction);
                            Intent returnIntent = new Intent();
                            setResult(DetailsActivity.RESULT_OK,returnIntent);
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.dialog_delete_transaction_cancel, null)
                    .show();
            return true;
        } else if (id == R.id.menu_edit) {
            EditDialogFragment ed = new EditDialogFragment();
            ed.setItem(transactionPosition);
            ed.show(getSupportFragmentManager(), EditDialogFragment.TAG);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTransactionEdited(Item newTransaction, int position) {
        ItemHolder.getInstance().editItem(newTransaction, position);

        Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.modify_success, Snackbar.LENGTH_LONG).show();
    }

    private void updateFields() {
        tvName.setText(transaction.getName());
        tvCategory.setText(transaction.getCategory().getName());
        imIsIncome.setImageResource(transaction.isIncome() ? R.drawable.up : R.drawable.down);
        SimpleDateFormat f = new SimpleDateFormat("yyyy. MM. dd.", Locale.getDefault());
        tvDate.setText(f.format(transaction.getDate().getTime()));
        tvComment.setText(transaction.getComment());
        tvValue.setText(getString(R.string.currency, transaction.getValue()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemAdded(Item item) {

    }

    @Override
    public void onItemDeleted(Item item) {
    }

    @Override
    public void onItemEdited(Item newItem, int position) {
        if (transactionPosition == position) {
            transaction = newItem;
            updateFields();
        }
    }
}
