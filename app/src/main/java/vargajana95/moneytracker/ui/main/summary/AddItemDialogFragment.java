package vargajana95.moneytracker.ui.main.summary;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.utils.listhelpers.CategoryIsIncomeFilter;
import vargajana95.moneytracker.utils.listhelpers.ListFilterHelper;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class AddItemDialogFragment extends AppCompatDialogFragment {
    public static final String TAG = "AddItemDialogFragment";


    private EditText etName;
    private EditText etValue;
    private EditText etComment;
    private Spinner spCategory;
    private RadioButton radioIncome;
    private DatePicker dpDate;
    private Calendar date;
    private ArrayList<Category> categories;

    public void setDate(Calendar date) {
        this.date = date;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getActivity() instanceof AddItemDialogListener) {
            listener = (AddItemDialogListener) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement AddItemDialogListener interface!");
        }*/

        categories = ListFilterHelper.filter(CategoryHolder.getInstance().getCategoryList(), new CategoryIsIncomeFilter(true));

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.dialog_add_title)
                .setView(getContentView())
                .setPositiveButton(R.string.dialog_ok, null)
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();

        //Added this to add validation. Don't want to close the dialog on OK immediately
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isValid()) {
                            ItemHolder.getInstance().addItem(getItem());
                            dialog.dismiss();

                        }
                    }
                });
            }
        });
        return dialog;
    }

    private Item getItem() {
        Item item = new Item();
        item.setName(etName.getText().toString());
        try {
            item.setValue(Integer.parseInt(etValue.getText().toString()));
        } catch (NumberFormatException e) {
            item.setValue(0);
        }
        item.setComment(etComment.getText().toString());
        item.setCategory(categories.get(spCategory.getSelectedItemPosition()));
        item.setIncome(radioIncome.isChecked());
        item.setDate(dpDate.getYear(), dpDate.getMonth(), dpDate.getDayOfMonth());
        return item;
    }

    private boolean isValid() {
        if (etName.getText().toString().length() == 0) {
            etName.setError(getString(R.string.error_no_name));
            return false;
        }

        try {
            Integer.parseInt(etValue.getText().toString());
        } catch (NumberFormatException e) {
            etValue.setError(getString(R.string.error_no_number));
            return false;
        }

        if (categories.isEmpty()) {
            //TODO show text here
            return false;
        }
        return true;
    }

    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_item, null);
        etName = view.findViewById(R.id.etName);
        etValue = view.findViewById(R.id.etValue);
        etComment = view.findViewById(R.id.etComment);
        spCategory = view.findViewById(R.id.spCategory);
        spCategory.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, ListFilterHelper.getCategoryNames(categories)));
        radioIncome = view.findViewById(R.id.radioIncome);
        radioIncome.setChecked(true);
        radioIncome.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                categories = ListFilterHelper.filter(CategoryHolder.getInstance().getCategoryList(), new CategoryIsIncomeFilter(b));
                spCategory.setAdapter(new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_spinner_dropdown_item, ListFilterHelper.getCategoryNames(categories)));
            }
        });
        dpDate = view.findViewById(R.id.dpDate);
        dpDate.updateDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH));
        return view;
    }



}
