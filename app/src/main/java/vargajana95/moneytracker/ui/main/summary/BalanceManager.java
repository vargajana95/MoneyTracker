package vargajana95.moneytracker.ui.main.summary;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.Observer;
import vargajana95.moneytracker.utils.Utils;
import vargajana95.moneytracker.utils.listhelpers.ListFilterHelper;
import vargajana95.moneytracker.utils.listhelpers.TransactionIntervalFilter;
import vargajana95.moneytracker.utils.listhelpers.TransactionIsIncomeFilter;
import vargajana95.moneytracker.utils.statisticshelper.StatisticsHelper;

/**
 * Created by Varga János on 2017. 10. 23..
 */

public class BalanceManager extends Fragment implements Observer {
    public static String TAG = "BalanceManager";

    private int balanceFrame;
    private int startingBalance;
    private Calendar startDate;
    private Calendar endDate;
    private BalanceListener listener;
    private boolean frameIsEnabled;
    private ProgressBar progressBar;
    private TextView tvBalanceLeft;
    private TextView tvBalanceSpent;
    private TextView tvBalance;
    private TextView tvBalanceText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.balance_manager_layout, parent, false);
        progressBar = view.findViewById(R.id.pbBalance);
        tvBalanceLeft = view.findViewById(R.id.tvBalanceLeft);
        tvBalanceSpent = view.findViewById(R.id.tvBalanceSpent);
        tvBalance = view.findViewById(R.id.tvBalance);
        tvBalanceText = view.findViewById(R.id.tvBalanceText);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ItemHolder.getInstance().addObserver(this);


        SharedPreferences Sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());

        //TODO add default value, or don't enable when date is not set
        long balanceStartDate = Sp.getLong("balanceStartDate", 0);
        Calendar startDate = Calendar.getInstance();
        startDate.setTimeInMillis(balanceStartDate);

        long balanceEndDate = Sp.getLong("balanceEndDate", 0);
        Calendar endDate = Calendar.getInstance();
        endDate.setTimeInMillis(balanceEndDate);
        setInterval(startDate, endDate);

        balanceFrame = Integer.parseInt(Sp.getString("balanceFrame", "0"));

        startingBalance = Integer.parseInt(Sp.getString("startingBalance", "0"));


        frameIsEnabled = Sp.getBoolean("balanceIsActive", false);

        if (frameIsEnabled && Calendar.getInstance().before(endDate) && Calendar.getInstance().after(startDate)) {
            checkFrame();
        } else
            checkBalance();
    }

    @Override
    public void onPause() {
        super.onPause();
        ItemHolder.getInstance().removeObserver(this);
    }

    public void setListener(BalanceListener listener) {
        this.listener = listener;
    }

    public void setInterval(Calendar start, Calendar end) {
        startDate = start;
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);

        endDate = end;
        endDate.set(Calendar.HOUR_OF_DAY, 23);
        endDate.set(Calendar.MINUTE, 59);
        endDate.set(Calendar.SECOND, 59);
    }


    public void checkFrame() {
        //Ha be van kapcsolva, akkor meg kell vizsgálni, hogy nem járt-e le a keret
        if (!(Calendar.getInstance().before(endDate) && Calendar.getInstance().after(startDate))) {
            frameIsEnabled = false;
            return;
        }
        /*if (!enabled || startDate == null || endDate == null)
            return;*/

        ItemHolder itemHolder = ItemHolder.getInstance();
        ArrayList<Item> transactions = ListFilterHelper.filter(itemHolder.getItems(),
                new TransactionIntervalFilter(startDate, endDate));


        int expenseSum = -StatisticsHelper.calculateSum(ListFilterHelper.filter(transactions,
                new TransactionIsIncomeFilter(false)));

        tvBalanceText.setText(getString(R.string.frame_text));
        tvBalance.setText(getString(R.string.currency, balanceFrame));
        if (balanceFrame - expenseSum > 0)
            tvBalanceLeft.setText(getString(R.string.currency, balanceFrame - expenseSum));
        else
            tvBalanceLeft.setText(getString(R.string.currency, 0));
        tvBalanceSpent.setText(getString(R.string.currency, expenseSum));

        progressBar.setMax(balanceFrame);
        progressBar.setProgress(balanceFrame - expenseSum);


        //If we spent more than the actual frame
        if (balanceFrame - expenseSum <= 0)
            listener.onFrameIsUsed();


    }

    void checkBalance() {
        ArrayList<Item> transactions = ItemHolder.getInstance().getItems();


        int expenseSum = -StatisticsHelper.calculateSum(ListFilterHelper.filter(transactions,
                new TransactionIsIncomeFilter(false)));

        int incomeSum = StatisticsHelper.calculateSum(ListFilterHelper.filter(transactions,
                new TransactionIsIncomeFilter(true)));

        tvBalanceText.setText(getString(R.string.balance_text));
        tvBalance.setText(getString(R.string.currency, startingBalance + incomeSum));
        if (startingBalance + incomeSum - expenseSum > 0)
            tvBalanceLeft.setText(getString(R.string.currency, startingBalance + incomeSum - expenseSum));
        else
            tvBalanceLeft.setText(getString(R.string.currency, 0));
        tvBalanceSpent.setText(getString(R.string.currency, expenseSum));

        progressBar.setMax(startingBalance + incomeSum);
        progressBar.setProgress(startingBalance + incomeSum - expenseSum);

        if (startingBalance + incomeSum - expenseSum <= 0)
            listener.onBalanceIsUsed();

    }

    @Override
    public void onItemAdded(Item item) {
        if (frameIsEnabled) {
            if (item.getDate().before(endDate) && item.getDate().after(startDate)) {
                checkFrame();
            }
        } else
            checkBalance();
    }

    @Override
    public void onItemDeleted(Item item) {
        if (frameIsEnabled) {
            if (item.getDate().before(endDate) && item.getDate().after(startDate)) {
                checkFrame();
            }
        } else
            checkBalance();
    }

    @Override
    public void onItemEdited(Item newItem, int position) {
        if (frameIsEnabled) {
            checkFrame();
        } else
            checkBalance();
    }

    public interface BalanceListener {
        void onBalanceIsUsed();
        void onFrameIsUsed();
    }
}
