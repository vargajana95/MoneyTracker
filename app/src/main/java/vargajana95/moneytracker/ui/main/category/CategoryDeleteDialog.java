package vargajana95.moneytracker.ui.main.category;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.RepeatingTransaction;
import vargajana95.moneytracker.utils.listhelpers.CategoryIsIncomeFilter;
import vargajana95.moneytracker.utils.listhelpers.Filter;
import vargajana95.moneytracker.utils.listhelpers.ListFilterHelper;
import vargajana95.moneytracker.utils.listhelpers.TransactionCategoryFilter;

/**
 * Created by Varga János on 2017. 11. 13..
 */

public class CategoryDeleteDialog extends AppCompatDialogFragment {
    public static final String TAG = "CategoryDeleteDialog";


    private OnCategoryDeletedListener listener;
    private Spinner spCategory;
    private Category category;
    private ArrayList<Category> categories;


    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof OnCategoryDeletedListener) {
            listener = (OnCategoryDeletedListener) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement OnCategoryDeletedListener interface!");
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.category_delete_dialog_title)
                .setView(getContentView())
                .setPositiveButton(R.string.dialog_ok, null)
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();

        //Added this to add validation. Don't want to close the dialog on OK immediately
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isValid()) {
                            moveTransactions();
                            listener.onCategoryDeleted(category);
                            dialog.dismiss();
                        }
                    }
                });
            }
        });


        return dialog;
    }

    private void moveTransactions() {
        ArrayList<Item> transactions = ListFilterHelper.filter(ItemHolder.getInstance().getItems(),
                                                new TransactionCategoryFilter(category));

        for(Item transaction : transactions) {
            transaction.setCategory(categories.get(spCategory.getSelectedItemPosition()));
            transaction.save();
        }

        ArrayList<RepeatingTransaction> rTransactions = ListFilterHelper.filter(ItemHolder.getInstance().getRepeatingTransactions()
                , new Filter<RepeatingTransaction>() {
                    @Override
                    public boolean needToAdd(RepeatingTransaction o) {
                        return o.getCategory().equals(category);
                    }
                });

        for (RepeatingTransaction rTransaction : rTransactions) {
            rTransaction.setCategory(categories.get(spCategory.getSelectedItemPosition()));
            rTransaction.save();
        }

    }

    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_delete_category, null);
        spCategory = view.findViewById(R.id.spCategory);

        categories = ListFilterHelper.filter(CategoryHolder.getInstance().getCategoryList(),
                                        new CategoryIsIncomeFilter(category.isIncome()));

        //Nem akajuk, hogy ugyan az benne legyen a listaban
        categories.remove(category);

        spCategory.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, ListFilterHelper.getCategoryNames(categories)));

        return view;
    }

    public interface OnCategoryDeletedListener {
        void onCategoryDeleted(Category category);
    }

    private boolean isValid() {
        if (spCategory.getSelectedItem() == null) {
            return false;
        }
        return true;
    }


}

