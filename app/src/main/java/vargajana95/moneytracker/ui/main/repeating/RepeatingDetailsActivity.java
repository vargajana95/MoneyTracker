package vargajana95.moneytracker.ui.main.repeating;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.RepeatingTransaction;
import vargajana95.moneytracker.ui.main.MainActivity;
import vargajana95.moneytracker.ui.main.details.EditDialogFragment;
import vargajana95.moneytracker.utils.Utils;

public class RepeatingDetailsActivity extends AppCompatActivity implements EditRepeatingDialogFragment.OnRepeatingEditedListener{
    private static final String TAG = "RepeatingDetailsActivity";
    public static final String EXTRA_REPEATING_INDEX= "extra.index";

    private RepeatingTransaction transaction;
    private TextView tvName;
    private TextView tvCategory;
    private ImageView imIsIncome;
    private TextView tvComment;
    private TextView tvValue;
    private TextView tvType;
    private TextView tvRepeatNum;
    private LinearLayout repeatNumBlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_repeating);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        int index = getIntent().getIntExtra(EXTRA_REPEATING_INDEX, 0);
        transaction = ItemHolder.getInstance().getRepeatingTransaction(index);
        setTitle(transaction.getName());

        tvName = (TextView) findViewById(R.id.tvName);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        imIsIncome = (ImageView) findViewById(R.id.imIsIncome);
        tvComment = (TextView) findViewById(R.id.tvComment);
        tvValue = (TextView) findViewById(R.id.tvValue);
        tvType = (TextView) findViewById(R.id.tvType);
        tvRepeatNum = (TextView) findViewById(R.id.tvRepeatNum);
        repeatNumBlock = (LinearLayout) findViewById(R.id.repeatNumBlock);

        updateFields();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_delete) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_delete_transaction_title)
                    .setMessage(R.string.dialog_delete_transaction_text)
                    .setPositiveButton(R.string.dialog_delete_transaction_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //TODO use startActivityForResult instead
                            ItemHolder.getInstance().removeRepeatingTransaction(transaction);
                            Intent closeIntent = new Intent(RepeatingDetailsActivity.this, RepeatingTransactionsActivity.class);
                            closeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(closeIntent);
                        }
                    })
                    .setNegativeButton(R.string.dialog_delete_transaction_cancel, null)
                    .show();
            return true;
        } else if (id == R.id.menu_edit) {
            EditRepeatingDialogFragment ed = new EditRepeatingDialogFragment();
            ed.setItem(transaction);
            ed.show(getSupportFragmentManager(), EditRepeatingDialogFragment.TAG);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRepeatingTransactionEdited(RepeatingTransaction transaction) {
        //TODO do it in the Item class
        this.transaction.setName(transaction.getItem().getName());
        this.transaction.setNextAdded(transaction.getNextAdded());
        this.transaction.setValue(transaction.getItem().getValue());
        this.transaction.setComment(transaction.getItem().getComment());
        this.transaction.setIncome(transaction.getItem().isIncome());
        this.transaction.setCategory(transaction.getItem().getCategory());
        this.transaction.setRepeatType(transaction.getRepeatType());
        this.transaction.setRepeatTimesEnabled(transaction.isRepeatTimesEnabled());
        this.transaction.setRepeatNum(transaction.getRepeatNum());


        updateFields();

        Snackbar.make(findViewById(R.id.coordinatorLayout), getString(R.string.modify_success), Snackbar.LENGTH_LONG).show();
    }

    private void updateFields() {
        tvName.setText(transaction.getName());
        tvCategory.setText(transaction.getCategory().getName());
        imIsIncome.setImageResource(transaction.isIncome() ? R.drawable.up : R.drawable.down);
        tvComment.setText(transaction.getComment());
        tvValue.setText(getString(R.string.currency, transaction.getValue()));
        tvType.setText(transaction.getRepeatTypeString());
        tvRepeatNum.setText(String.format(Locale.getDefault(), "%d",transaction.getRepeatNum()));
        if (transaction.isRepeatTimesEnabled())
            repeatNumBlock.setVisibility(View.VISIBLE);
        else
            repeatNumBlock.setVisibility(View.GONE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

