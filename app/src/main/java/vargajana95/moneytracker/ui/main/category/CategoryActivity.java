package vargajana95.moneytracker.ui.main.category;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.ui.main.MainActivity;
import vargajana95.moneytracker.ui.main.summary.AddItemDialogFragment;

public class CategoryActivity extends AppCompatActivity
        implements AddCategoryDialogFragment.OnCategoryAddedListener, CategoryAdapter.OnCategorySelectedListener, EditCategoryDialogFragment.OnCategoryEditedListener,
        CategoryDeleteDialog.OnCategoryDeletedListener{

    private RecyclerView recyclerView;
    private CategoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        initRecyclerView();
    }


    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.CategoryRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CategoryAdapter(this);
        adapter.setList(CategoryHolder.getInstance().getCategoryList());

        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_categories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_add_category) {
            new AddCategoryDialogFragment().show(getSupportFragmentManager(), AddCategoryDialogFragment.TAG);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCategoryAdded(Category category) {
        CategoryHolder.getInstance().addCategory(category);
        adapter.categoryAdded(category);
    }


    @Override
    public void onCategoryEdit(Category category) {
        EditCategoryDialogFragment ed = new EditCategoryDialogFragment();
        ed.setCategory(category);
        ed.show(getSupportFragmentManager(), EditCategoryDialogFragment.TAG);
    }

    @Override
    public void onCategoryDelete(Category category) {
        CategoryDeleteDialog dialog = new CategoryDeleteDialog();
        dialog.setCategory(category);
        dialog.show(getSupportFragmentManager(), CategoryDeleteDialog.TAG);
    }

    //TODO get the position as only parameter
    @Override
    public void onCategoryEdited(Category category) {
        category.save();

        adapter.notifyItemChanged(CategoryHolder.getInstance().getCategoryPosition(category));
        //TODO remove
        Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.modify_success, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onCategoryDeleted(Category category) {
        CategoryHolder.getInstance().removeCategory(category);
        adapter.categoriesChanged(CategoryHolder.getInstance().getCategoryList());
    }
}
