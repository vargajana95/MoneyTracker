package vargajana95.moneytracker.ui.main.statistics;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.StatisticsItem;
import vargajana95.moneytracker.utils.Utils;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ItemViewHolder> {
    private  ArrayList<StatisticsItem> statistics;
    private Context context;

    public StatisticsAdapter(Context context) {
        this.context = context;
        statistics = new ArrayList<>();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stat_layout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.position = position;
        holder.tvCategory.setText(statistics.get(position).category.getName());
        holder.tvPercent.setText(context.getString(R.string.percent, (int) Math.round(statistics.get(position).percent)));
        holder.pbPercent.setProgress((int) Math.round(statistics.get(position).percent));
        holder.tvCategorySum.setText(context.getString(R.string.currency, statistics.get(position).sum));
    }

    @Override
    public int getItemCount() {
        return statistics.size();
    }

    public void itemsChanged(ArrayList<StatisticsItem> statistics) {
        this.statistics = (ArrayList<StatisticsItem>) statistics.clone();
        notifyDataSetChanged();
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private TextView tvCategory;
        private TextView tvPercent;
        private ProgressBar pbPercent;
        private TextView tvCategorySum;
        public ItemViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvPercent = itemView.findViewById(R.id.tvPercent);
            pbPercent = itemView.findViewById(R.id.pbPercent);
            tvCategorySum = itemView.findViewById(R.id.tvCategorySum);

        }
    }

}
