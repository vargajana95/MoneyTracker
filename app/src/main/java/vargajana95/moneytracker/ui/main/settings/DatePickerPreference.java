package vargajana95.moneytracker.ui.main.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;

import vargajana95.moneytracker.R;

/**
 * Created by Varga János on 2017. 10. 24..
 */

public class DatePickerPreference extends DialogPreference {
    private DatePicker datePicker;
    private long date;

    public DatePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.datepicker_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        setDialogIcon(null);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        datePicker = view.findViewById(R.id.dialog_date_picker);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        datePicker.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            // Restore existing state
            date = this.getPersistedLong(0);
        }
        else {
            date = Calendar.getInstance().getTimeInMillis();
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return Calendar.getInstance().getTimeInMillis();
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {

        if (positiveResult) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
            date = calendar.getTimeInMillis();
            persistLong(date);
        }
    }
}
