package vargajana95.moneytracker.ui.main.summary;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.OnRepeatingTransactionsAddedListener;
import vargajana95.moneytracker.utils.Utils;

/**
 * Created by Varga János on 2017. 10. 21..
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private ArrayList<Item> items;
    private OnTransactionSelectedListener listener;
    private Context context;

    public ItemAdapter(Context context, OnTransactionSelectedListener listener) {
        this.context=  context;
        items = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        ItemViewHolder holder = new ItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.position = position;
        holder.tvName.setText(items.get(position).getName());
        if (items.get(position).isIncome())
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
        else
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
        holder.tvValue.setText(context.getString(R.string.currency, items.get(position).getValue()));
        holder.tvCategory.setText(items.get(position).getCategory().getName());
        holder.tvIsIncome.setImageResource(items.get(position).isIncome() ? R.drawable.up : R.drawable.down);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void itemsChanged(ArrayList<Item> items) {
        this.items = (ArrayList<Item>) items.clone();
        notifyDataSetChanged();
    }

    public void ItemAdded(Item item) {
        items.add(item);
        notifyItemInserted(items.size()-1);
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private TextView tvName;
        private TextView tvValue;
        private TextView tvCategory;
        private ImageView tvIsIncome;
        public ItemViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvValue = itemView.findViewById(R.id.tvValue);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvIsIncome = itemView.findViewById(R.id.tvIsIncome);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onTransactionSelected(items.get(position));
                    }
                }
            });
        }
    }

    public interface OnTransactionSelectedListener {
        void onTransactionSelected(Item transaction);
    }

}
