package vargajana95.moneytracker.ui.main.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import vargajana95.moneytracker.R;

/**
 * Created by Varga János on 2017. 11. 08..
 */

public class BalanceSettingsFragment  extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.balance_settings);
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().setTitle(getString(R.string.balance_settings));
    }
}
