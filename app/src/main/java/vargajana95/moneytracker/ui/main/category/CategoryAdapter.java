package vargajana95.moneytracker.ui.main.category;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;

/**
 * Created by Varga János on 2017. 10. 28..
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private ArrayList<Category> categories;
    private OnCategorySelectedListener listener;

    public CategoryAdapter(OnCategorySelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position;
        holder.tvName.setText(categories.get(holder.getAdapterPosition()).getName());
        holder.imIsIncome.setImageResource(categories.get(position).isIncome() ? R.drawable.up : R.drawable.down);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setList(ArrayList<Category> list) {
        this.categories = list;
        notifyDataSetChanged();
    }

    public void categoryAdded(Category category) {
        categories.add(category);
        notifyItemInserted(categories.size()-1);
    }
    public void categoriesChanged(ArrayList<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private TextView tvName;
        private ImageView imIsIncome;
        private ImageButton btnEdit;
        private ImageButton btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            imIsIncome = itemView.findViewById(R.id.imIsIncome);
            btnEdit =  itemView.findViewById(R.id.btnEdit);
            btnDelete =  itemView.findViewById(R.id.btnDelete);

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onCategoryEdit(categories.get(position));
                }
            });
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onCategoryDelete(categories.get(position));
                }
            });
        }

    }

    public interface OnCategorySelectedListener {
        void onCategoryEdit(Category category);
        void onCategoryDelete(Category category);
    }
}
