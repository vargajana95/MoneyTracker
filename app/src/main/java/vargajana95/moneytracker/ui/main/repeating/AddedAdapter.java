package vargajana95.moneytracker.ui.main.repeating;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.RepeatingTransactionToAdd;

/**
 * Created by Varga János on 2017. 10. 27..
 */

public class AddedAdapter
        extends RecyclerView.Adapter<AddedAdapter.ViewHolder> {
    private ArrayList<RepeatingTransactionToAdd> addedTransactions;

    public AddedAdapter() {
        addedTransactions = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.added_transaction_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position;
        holder.chAdd.setChecked(addedTransactions.get(position).isChecked());
        holder.tvCategory.setText(addedTransactions.get(position).getItem().getCategory().getName());
        holder.tvName.setText(addedTransactions.get(position).getItem().getName());
        SimpleDateFormat f = new SimpleDateFormat("MM/dd", Locale.getDefault());
        holder.tvDate.setText(f.format(addedTransactions.get(position).getItem().getDate().getTime()));
    }

    @Override
    public int getItemCount() {
        return addedTransactions.size();
    }

    public void setList(ArrayList<RepeatingTransactionToAdd> list) {
        this.addedTransactions = list;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private CheckBox chAdd;
        private TextView tvCategory;
        private TextView tvName;
        private TextView tvDate;
        public ViewHolder(View itemView) {
            super(itemView);
            chAdd = itemView.findViewById(R.id.chAdd);
            chAdd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    addedTransactions.get(position).setChecked(chAdd.isChecked());
                }
            });
            tvDate = itemView.findViewById(R.id.tvDate);
            tvName = itemView.findViewById(R.id.tvName);
            tvCategory = itemView.findViewById(R.id.tvCategory);
        }
    }

}
