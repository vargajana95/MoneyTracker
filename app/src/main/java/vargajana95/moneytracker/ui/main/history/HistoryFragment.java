package vargajana95.moneytracker.ui.main.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.Observer;
import vargajana95.moneytracker.ui.main.details.DetailsActivity;
import vargajana95.moneytracker.ui.main.summary.ItemAdapter;
import vargajana95.moneytracker.utils.Utils;
import vargajana95.moneytracker.utils.listhelpers.ListFilterHelper;
import vargajana95.moneytracker.utils.listhelpers.TransactionCategoryFilter;
import vargajana95.moneytracker.utils.listhelpers.TransactionDateComparator;


/**
 * Created by Varga János on 2017. 10. 21..
 */

public class HistoryFragment extends Fragment implements Observer {
    private RecyclerView recyclerView;
    private ItemAdapter itemAdapter;
    private Calendar filterDate;
    private int filterDateType;
    private Category filterCategory;
    private Button btnDate;
    private ImageButton btnPrev;
    private ImageButton btnNext;
    private boolean filterAllDates;

    ArrayList<Item> items;
    ItemHolder itemHolder;

    public HistoryFragment() {
        filterDate = Calendar.getInstance();

        filterDateType = Calendar.WEEK_OF_YEAR;

        itemHolder = ItemHolder.getInstance();

        items = new ArrayList<>();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.findItem(R.id.menu_category).setVisible(true);
        menu.findItem(R.id.menu_date).setVisible(true);
        menu.findItem(R.id.menu_add).setVisible(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        btnPrev = view.findViewById(R.id.btnPrevDay);
        btnNext = view.findViewById(R.id.btnNextDay);
        btnDate = view.findViewById(R.id.btnDate);

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDate.add(filterDateType, -1);
                filterTransactions();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDate.add(filterDateType, 1);
                filterTransactions();
            }
        });


        initRecyclerView(view);

        itemHolder.addObserver(this);

        filterTransactions();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        itemHolder.removeObserver(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DetailsActivity.DELETE_RESULT) {
            if (resultCode == DetailsActivity.RESULT_OK) {
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), R.string.delete_success, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }


    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.HistoryRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        itemAdapter = new ItemAdapter(this.getContext(), new ItemAdapter.OnTransactionSelectedListener() {
            @Override
            public void onTransactionSelected(Item transaction) {
                int index = itemHolder.getItemIndex(transaction);
                Intent showDetailsIntent = new Intent(getActivity(), DetailsActivity.class);
                showDetailsIntent.putExtra(DetailsActivity.EXTRA_TRANSACTION_INDEX, index);
                startActivityForResult(showDetailsIntent, DetailsActivity.DELETE_RESULT);
            }
        });
        itemAdapter.itemsChanged(items);


        recyclerView.setAdapter(itemAdapter);
    }


    private void filterTransactions() {
        if (!filterAllDates) {
            items = itemHolder.getItemsByType(filterDateType, filterDate, filterCategory);
            //items.sort(new TransactionDateComparator());
            Collections.sort(items, new TransactionDateComparator());
            btnNext.setEnabled(true);
            btnPrev.setEnabled(true);
            if (filterDateType == Calendar.MONTH)
                btnDate.setText(filterDate.getDisplayName(filterDateType, Calendar.LONG, Locale.getDefault()));
            else if (filterDateType == Calendar.WEEK_OF_YEAR) {
                Calendar weekBegin = (Calendar) filterDate.clone();
                weekBegin.set(Calendar.HOUR_OF_DAY, 0);
                weekBegin.clear(Calendar.MINUTE);
                weekBegin.clear(Calendar.SECOND);
                weekBegin.clear(Calendar.MILLISECOND);

                weekBegin.set(Calendar.DAY_OF_WEEK, weekBegin.getFirstDayOfWeek());

                Calendar weekEnd = (Calendar) weekBegin.clone();
                weekEnd.add(Calendar.WEEK_OF_YEAR, 1);
                weekEnd.add(Calendar.DAY_OF_WEEK, -1);
                //btnDate.setText(getString(R.string.week_of_year, filterDate.get(filterDateType)));
                btnDate.setText(weekBegin.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " +
                        weekBegin.get(Calendar.DAY_OF_MONTH) + " - " +
                        weekEnd.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " +
                        weekEnd.get(Calendar.DAY_OF_MONTH));
            }
            else
                btnDate.setText(String.format(Locale.getDefault(), "%d", filterDate.get(filterDateType)));
        } else {
            items = ListFilterHelper.filter(ItemHolder.getInstance().getItems(),
                    new TransactionCategoryFilter(filterCategory));

            Collections.sort(items, new TransactionDateComparator());
            btnDate.setText(R.string.list_all);
            btnNext.setEnabled(false);
            btnPrev.setEnabled(false);
        }

        itemAdapter.itemsChanged(items);
    }


    @Override
    public void onItemAdded(Item item) {
        //TODO csak akkor, ha ugyan az a kategoria es a datum relevans resze
        filterTransactions();
        itemAdapter.itemsChanged(items);
    }

    @Override
    public void onItemDeleted(Item item) {
        filterTransactions();
        itemAdapter.itemsChanged(items);
    }

    @Override
    public void onItemEdited(Item newItem, int position) {
        filterTransactions();
        itemAdapter.itemsChanged(items);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_list_all) {
            filterAllDates = true;
            filterTransactions();
            return true;
        } else if (id == R.id.action_year) {
            filterDateType = Calendar.YEAR;
            filterAllDates = false;
            filterTransactions();
            return true;
        } else if (id == R.id.action_month) {
            filterDateType = Calendar.MONTH;
            filterAllDates = false;
            filterTransactions();
            return true;
        } else if (id == R.id.action_week) {
            filterDateType = Calendar.WEEK_OF_YEAR;
            filterAllDates = false;
            filterTransactions();
            return true;
        }
        if (id == R.id.menu_category) {
            View v = getActivity().findViewById(R.id.menu_category);
            showCategoryMenu(v);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void showCategoryMenu(View v) {
        //PopupMenu popup = new PopupMenu(this, v);
        PopupMenu popup = new PopupMenu(getActivity(), v, Gravity.NO_GRAVITY, R.attr.actionOverflowMenuStyle, 0);

        popup.getMenu().add(getString(R.string.list_all)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                filterCategory = null;
                filterTransactions();
                return true;
            }
        });
        for (String category : CategoryHolder.getInstance().getCategories()) {
            popup.getMenu().add(category).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    filterCategory = CategoryHolder.getInstance().get(menuItem.getTitle().toString());
                    filterTransactions();
                    return true;
                }
            });
        }

        popup.show();
    }
}
