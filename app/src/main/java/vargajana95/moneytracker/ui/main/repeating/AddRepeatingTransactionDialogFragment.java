package vargajana95.moneytracker.ui.main.repeating;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.RepeatingTransaction;
import vargajana95.moneytracker.utils.listhelpers.CategoryIsIncomeFilter;
import vargajana95.moneytracker.utils.listhelpers.ListFilterHelper;

/**
 * Created by Varga János on 2017. 10. 25..
 */

public class AddRepeatingTransactionDialogFragment extends AppCompatDialogFragment {
    public static final String TAG = "AddRepeatingTransactionDialogFragment";


    private AddRepeatingTransactionListener listener;
    private EditText etName;
    private EditText etValue;
    private EditText etComment;
    private EditText etRepeatNum;
    private Spinner spCategory;
    private CheckBox cbRepeat;
    private RadioButton radioIncome;
    private DatePicker dpDate;
    private Spinner spInterval;
    private ArrayList<Category> categories;


    private enum Interval {
        WEEKLY, MONTHLY, YEARLY;

        public static Interval getByOrdinal(int ordinal) {
            Interval ret = null;
            for (Interval i : Interval.values()) {
                if (i.ordinal() == ordinal) {
                    ret = i;
                    break;
                }
            }
            return ret;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof AddRepeatingTransactionListener) {
            listener = (AddRepeatingTransactionListener) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement AddRepeatingTransactionListener interface!");
        }

        categories = ListFilterHelper.filter(CategoryHolder.getInstance().getCategoryList(), new CategoryIsIncomeFilter(true));

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.dialog_add_title)
                .setView(getContentView())
                .setPositiveButton(R.string.dialog_ok, null)
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();

        //Added this to add validation. Don't want to close the dialog on OK immediately
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isValid()) {
                            listener.onRepeatingTransactionAdded(getTransaction());
                            dialog.dismiss();

                        }
                    }
                });
            }
        });
        return dialog;
    }

    private RepeatingTransaction getTransaction() {
        Item item = new Item();
        item.setName(etName.getText().toString());
        try {
            item.setValue(Integer.parseInt(etValue.getText().toString()));
        } catch (NumberFormatException e) {
            item.setValue(0);
        }
        item.setComment(etComment.getText().toString());
        item.setCategory(categories.get(spCategory.getSelectedItemPosition()));
        item.setIncome(radioIncome.isChecked());
        RepeatingTransaction transaction = new RepeatingTransaction(item);
        Calendar cal = Calendar.getInstance();
        cal.set(dpDate.getYear(), dpDate.getMonth(), dpDate.getDayOfMonth());
        transaction.setNextAdded(cal);
        transaction.setRepeatType(getType(Interval.getByOrdinal(spInterval.getSelectedItemPosition())));

        transaction.setRepeatTimesEnabled(cbRepeat.isChecked());
        if (cbRepeat.isChecked()) {
            transaction.setRepeatNum(Integer.parseInt(etRepeatNum.getText().toString()));
        }
        return transaction;
    }

    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_repeating, null);
        etName = view.findViewById(R.id.etName);
        etValue = view.findViewById(R.id.etValue);
        etRepeatNum = view.findViewById(R.id.etRepeatNum);
        etComment = view.findViewById(R.id.etComment);
        spCategory = view.findViewById(R.id.spCategory);
        spCategory.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, ListFilterHelper.getCategoryNames(categories)));
        radioIncome = view.findViewById(R.id.radioIncome);
        radioIncome.setChecked(true);
        radioIncome.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                categories = ListFilterHelper.filter(CategoryHolder.getInstance().getCategoryList(), new CategoryIsIncomeFilter(b));
                spCategory.setAdapter(new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_spinner_dropdown_item, ListFilterHelper.getCategoryNames(categories)));
            }
        });
        dpDate = view.findViewById(R.id.dpDate);
        spInterval = view.findViewById(R.id.spInterval);
        spInterval.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.interval_items)));
        cbRepeat = view.findViewById(R.id.cbRepeat);
        etRepeatNum.setEnabled(cbRepeat.isChecked());
        cbRepeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                etRepeatNum.setEnabled(b);
            }
        });
        return view;
    }

    private int getType(Interval interval) {
        int ret;

        switch (interval) {
            case WEEKLY:
                ret = Calendar.WEEK_OF_YEAR;
                break;
            case MONTHLY:
                ret = Calendar.MONTH;
                break;
            case YEARLY:
                ret = Calendar.YEAR;
                break;
            default:
                ret = Calendar.WEEK_OF_YEAR;
                break;
        }
        return ret;
    }

    private boolean isValid() {
        if (etName.getText().toString().length() == 0) {
            etName.setError(getString(R.string.error_no_name));
            return false;
        }

        try {
            Integer.parseInt(etValue.getText().toString());
        } catch (NumberFormatException e) {
            etValue.setError(getString(R.string.error_no_number));
            return false;
        }

        if (cbRepeat.isChecked()) {
            try {
                Integer.parseInt(etRepeatNum.getText().toString());
            } catch (NumberFormatException e) {
                etRepeatNum.setError(getString(R.string.error_no_number));
                return false;
            }

            if (Integer.parseInt(etRepeatNum.getText().toString()) <= 0) {
                etRepeatNum.setError(getString(R.string.repeat_num_error));
                return false;
            }
        }

        if (categories.isEmpty()) {
            //TODO show text here
            return false;
        }
        return true;
    }

    public interface AddRepeatingTransactionListener {
        void onRepeatingTransactionAdded(RepeatingTransaction transaction);
    }


}

