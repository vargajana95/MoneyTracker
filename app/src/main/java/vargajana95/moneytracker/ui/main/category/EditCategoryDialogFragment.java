package vargajana95.moneytracker.ui.main.category;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;


import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;

/**
 * Created by Varga János on 2017. 11. 01..
 */


public class EditCategoryDialogFragment extends AppCompatDialogFragment {
    public static final String TAG = "EditCategoryDialogFragment";


    private OnCategoryEditedListener listener;
    private EditText etName;
    private RadioButton radioIncome;
    private Category category;


    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof OnCategoryEditedListener) {
            listener = (OnCategoryEditedListener) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement OnCategoryEditedListener interface!");
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.edit_dialog_title)
                .setView(getContentView())
                .setPositiveButton(R.string.dialog_ok, null)
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();

        //Added this to add validation. Don't want to close the dialog on OK immediately
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isValid()) {
                            editCategory();
                            listener.onCategoryEdited(category);
                            dialog.dismiss();

                        }
                    }
                });
            }
        });


        return dialog;
    }

    private void editCategory() {

        category.setName(etName.getText().toString());

        category.setIncome(radioIncome.isChecked());
    }

    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_new_category, null);
        etName = view.findViewById(R.id.etName);

        radioIncome = view.findViewById(R.id.radioIncome);
        RadioButton radioExpense = view.findViewById(R.id.radioExpense);

        etName.setText(category.getName());


        radioIncome.setChecked(category.isIncome());
        radioExpense.setChecked(!category.isIncome());

        return view;
    }

    public interface OnCategoryEditedListener {
        void onCategoryEdited(Category category);
    }

    private boolean isValid() {
        if (etName.getText().toString().length() == 0) {
            etName.setError(getString(R.string.error_no_name));
            return false;
        }
        return true;
    }


}
