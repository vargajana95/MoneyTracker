package vargajana95.moneytracker.ui.main.repeating;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.RepeatingTransactionToAdd;

/**
 * Created by Varga János on 2017. 10. 27..
 */

public class RepeatingTransactionsAddedDialogFragment extends AppCompatDialogFragment {
    public static final String TAG = "RepeatingTransactionDialogAddedFragment";
    private RecyclerView recyclerView;
    private AddedAdapter adapter;
    private ArrayList<RepeatingTransactionToAdd> transactions;

    public RepeatingTransactionsAddedDialogFragment() {
        adapter = new AddedAdapter();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.add_repeating_transaction_dialog_title)
                .setView(getContentView())
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (RepeatingTransactionToAdd transaction : transactions) {
                            if (transaction.isChecked())
                                ItemHolder.getInstance().addItem(transaction.getItem());
                        }
                    }
                })
                .create();
    }

    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.AddedTransactionsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        recyclerView.setAdapter(adapter);
    }

    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_added_item, null);
        initRecyclerView(view);
        return view;
    }

    public void setList(ArrayList<RepeatingTransactionToAdd> transactions) {
        this.transactions = transactions;
        adapter.setList(transactions);
    }

}

