package vargajana95.moneytracker.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.SubMenu;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Category;
import vargajana95.moneytracker.model.CategoryHolder;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.OnRepeatingTransactionsAddedListener;
import vargajana95.moneytracker.model.RepeatingTransactionToAdd;
import vargajana95.moneytracker.model.RepeatingTransactionsManager;
import vargajana95.moneytracker.ui.main.history.HistoryFragment;
import vargajana95.moneytracker.ui.main.repeating.RepeatingTransactionsAddedDialogFragment;
import vargajana95.moneytracker.ui.main.settings.SettingsActivity;
import vargajana95.moneytracker.ui.main.statistics.StatisticsFragment;
import vargajana95.moneytracker.ui.main.summary.MainFragment;

public class MainActivity extends AppCompatActivity implements OnRepeatingTransactionsAddedListener {

    private static final String KEY_FIRST_TIME = "isFirstTime";

    private RepeatingTransactionsManager repManager;

    private DrawerLayout mDrawer;
    private NavigationView navigationView;
    private ActionBarDrawerToggle drawerToggle;
    private int currentMenuItem;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.DrawerTheme);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences Sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean isFirstTime = Sp.getBoolean(KEY_FIRST_TIME, true);

        if (isFirstTime) {
            SharedPreferences.Editor editor = Sp.edit();
            editor.putBoolean(KEY_FIRST_TIME, false);
            editor.commit();
            //TODO delete this
            CategoryHolder.getInstance().addCategory(new Category("Bevásárlás"));
            CategoryHolder.getInstance().addCategory(new Category("Iskola"));
            CategoryHolder.getInstance().addCategory(new Category("Étkezés"));
            CategoryHolder.getInstance().addCategory(new Category("Szórakozás"));
            CategoryHolder.getInstance().addCategory(new Category("Ösztöndíj", true));
            CategoryHolder.getInstance().addCategory(new Category("Fizetés", true));
        }


        repManager = new RepeatingTransactionsManager(this);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddItemDialogFragment a = new AddItemDialogFragment();
                a.setDate(mainFragment.getFilterDate());
                a.show(getSupportFragmentManager(), AddItemDialogFragment.TAG);
            }
        });*/


        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setupDrawerContent(navigationView);
        currentMenuItem = R.id.nav_summary;
        setTitle(getString(R.string.summary_fragment_name));



        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentManager.beginTransaction().replace(R.id.flContent,  MainFragment.class.newInstance()).commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateNavBarSelection(currentMenuItem);
        repManager.check();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navView) {
        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    //TODO boolean-al kellene visszatrni?
    public void selectDrawerItem(MenuItem menuItem) {

        int id = menuItem.getItemId();

        if (id == R.id.nav_summary) {
            changeFragment(MainFragment.class, menuItem.getTitle().toString());
            currentMenuItem = id;
        } else if (id == R.id.nav_history) {
            changeFragment(HistoryFragment.class, menuItem.getTitle().toString());
            currentMenuItem = id;
        } else if (id == R.id.nav_statistics) {
            changeFragment(StatisticsFragment.class, menuItem.getTitle().toString());
            currentMenuItem = id;
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        updateNavBarSelection(id);


        // Highlight the selected item has been done by NavigationView
        //menuItem.setChecked(true);
        // Set action bar title
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    private void changeFragment(Class fragmentClass, String name) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        setTitle(name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (currentMenuItem != R.id.nav_summary) {
            changeFragment(MainFragment.class, getString(R.string.summary_fragment_name));
            updateNavBarSelection(R.id.nav_summary);
        } else
            super.onBackPressed();
    }

    private void updateNavBarSelection(int id) {
        //Deselect the other menuItems
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setChecked(false);

            if (menu.getItem(i).hasSubMenu()) {
                final SubMenu subMenu = menu.getItem(i).getSubMenu();
                for (int j = 0; j < subMenu.size(); j++) {
                    subMenu.getItem(j).setChecked(false);
                }
            }
        }
        navigationView.getMenu().findItem(id).setChecked(true);
    }


    @Override
    public void onRepeatingTransactionsAdded(ArrayList<RepeatingTransactionToAdd> items) {
        RepeatingTransactionsAddedDialogFragment addDialog = new RepeatingTransactionsAddedDialogFragment();
        addDialog.setList(items);
        addDialog.setCancelable(false);
        addDialog.show(getSupportFragmentManager(), RepeatingTransactionsAddedDialogFragment.TAG);
    }




}
