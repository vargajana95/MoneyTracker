package vargajana95.moneytracker.ui.main.repeating;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.Item;
import vargajana95.moneytracker.model.RepeatingTransaction;
import vargajana95.moneytracker.utils.Utils;

/**
 * Created by Varga János on 2017. 10. 25..
 */

public class RepeatingTransactionsAdapter
        extends RecyclerView.Adapter<RepeatingTransactionsAdapter.RepeatingTransactionsViewHolder> {
    private ArrayList<RepeatingTransaction> repeatingTransactions;
    private OnRepeatingSelectedListener listener;

    private Context context;

    public RepeatingTransactionsAdapter(Context context, OnRepeatingSelectedListener listener) {
        this.listener = listener;
        repeatingTransactions = new ArrayList<>();
        this.context = context;
    }

    @Override
    public RepeatingTransactionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repeating_transaction_layout, parent, false);
        RepeatingTransactionsViewHolder holder = new RepeatingTransactionsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RepeatingTransactionsViewHolder holder, int position) {
        holder.position = position;
        holder.tvName.setText(repeatingTransactions.get(position).getName());
        holder.tvValue.setText(context.getString(R.string.currency, repeatingTransactions.get(position).getValue()));
        holder.tvCategory.setText(repeatingTransactions.get(position).getCategory().getName());
        holder.imIsIncome.setImageResource(repeatingTransactions.get(position).isIncome() ? R.drawable.up : R.drawable.down);
        holder.tvRepeatType.setText(repeatingTransactions.get(position).getRepeatTypeString());
    }

    @Override
    public int getItemCount() {
        return repeatingTransactions.size();
    }

    public void transactionAdded(RepeatingTransaction transaction) {
        repeatingTransactions.add(transaction);
        notifyItemInserted(repeatingTransactions.size()-1);
    }

    public void setList(ArrayList<RepeatingTransaction> repeatingTransactions)
    {
        this.repeatingTransactions = repeatingTransactions;
        notifyDataSetChanged();
    }


    public class RepeatingTransactionsViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private TextView tvName;
        private TextView tvValue;
        private TextView tvCategory;
        private ImageView imIsIncome;
        private TextView tvRepeatType;
        public RepeatingTransactionsViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvValue = itemView.findViewById(R.id.tvValue);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            imIsIncome = itemView.findViewById(R.id.imIsIncome);
            tvRepeatType = itemView.findViewById(R.id.tvRepeatType);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onRepeatingSelected(repeatingTransactions.get(position));
                    }
                }
            });
        }
    }

    public interface OnRepeatingSelectedListener {
        void onRepeatingSelected(RepeatingTransaction transaction);
    }

}
