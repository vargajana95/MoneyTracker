package vargajana95.moneytracker.ui.main.repeating;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import vargajana95.moneytracker.R;
import vargajana95.moneytracker.model.ItemHolder;
import vargajana95.moneytracker.model.RepeatingTransaction;

public class RepeatingTransactionsActivity extends AppCompatActivity implements AddRepeatingTransactionDialogFragment.AddRepeatingTransactionListener {

    private RecyclerView recyclerView;
    private RepeatingTransactionsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeating_transactions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        initRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        adapter.setList(ItemHolder.getInstance().getRepeatingTransactions());
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.RepeatingTransactionsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RepeatingTransactionsAdapter(this, new RepeatingTransactionsAdapter.OnRepeatingSelectedListener() {
            @Override
            public void onRepeatingSelected(RepeatingTransaction transaction) {
                int index = ItemHolder.getInstance().getRepeatingTransactionIndex(transaction);
                Intent showDetailsIntent = new Intent(RepeatingTransactionsActivity.this, RepeatingDetailsActivity.class);
                showDetailsIntent.putExtra(RepeatingDetailsActivity.EXTRA_REPEATING_INDEX, index);
                startActivity(showDetailsIntent);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_repeating_transactions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_add_repeating) {
            AddRepeatingTransactionDialogFragment addDialog = new AddRepeatingTransactionDialogFragment();
            addDialog.show(getSupportFragmentManager(), AddRepeatingTransactionDialogFragment.TAG);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRepeatingTransactionAdded(RepeatingTransaction transaction) {
        adapter.transactionAdded(transaction);
        ItemHolder.getInstance().addRepeatingTransaction(transaction);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
